#include <windows.h>

const LPCSTR CONTROL_TEXT = "[CTRL]";
const LPCSTR ALT_TEXT = "[ALT]";
const LPCSTR SHIFT_TEXT = "[SHIFT]";

static HWND ctrlHandle = NULL;
static HWND altHandle = NULL;
static HWND shiftHandle = NULL;
static HWND charHandle = NULL;

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

LRESULT CALLBACK KeyboardProc(int code, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE instance, HINSTANCE previousInstance, PSTR cmdLine, int cmdShow) {
    WNDCLASSEXW wndClass;
    wndClass.cbSize = sizeof(WNDCLASSEXW);
    wndClass.style = CS_HREDRAW | CS_VREDRAW;
    wndClass.lpfnWndProc = WndProc;
    wndClass.cbClsExtra = 0;
    wndClass.cbWndExtra = 0;
    wndClass.hInstance = instance;
    wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndClass.hCursor = LoadIcon(NULL, IDC_ARROW);
    wndClass.hbrBackground = GetSysColorBrush(COLOR_3DFACE);
    wndClass.lpszMenuName = NULL;
    wndClass.lpszClassName = (LPCWSTR) L"Window";

    RegisterClassExW(&wndClass);

    HWND hwnd = CreateWindowW(wndClass.lpszClassName, (LPWSTR) L"Keymap",
                              WS_OVERLAPPEDWINDOW | WS_VISIBLE,
                              100, 100, 200, 250,
                              NULL, NULL, instance, NULL);

    ShowWindow(hwnd, cmdShow);
    UpdateWindow(hwnd);

    HHOOK keyboardHook = SetWindowsHookEx(WH_KEYBOARD_LL, KeyboardProc, 0, 0);

    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    UnhookWindowsHookEx(keyboardHook);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    switch (msg) {
        case WM_CREATE:
            ctrlHandle = CreateWindow("Static", CONTROL_TEXT,
                                      WS_CHILD | WS_VISIBLE | SS_CENTER,
                                      20, 20, 50, 20,
                                      hwnd, (HMENU) 1, NULL, NULL);
            altHandle = CreateWindow("Static", ALT_TEXT,
                                     WS_CHILD | WS_VISIBLE | SS_CENTER,
                                     70, 20, 50, 20,
                                     hwnd, (HMENU) 1, NULL, NULL);
            shiftHandle = CreateWindow("Static", SHIFT_TEXT,
                                       WS_CHILD | WS_VISIBLE | SS_CENTER,
                                       120, 20, 50, 20,
                                       hwnd, (HMENU) 1, NULL, NULL);
            charHandle = CreateWindow("STATIC", "",
                                      WS_CHILD | WS_VISIBLE | SS_CENTER,
                                      20, 40, 160, 180,
                                      hwnd, (HMENU) 1, NULL, NULL);
            HDC hdc = GetDC(hwnd);
            int height = -MulDiv(100, GetDeviceCaps(hdc, LOGPIXELSY), 72);
            ReleaseDC(hwnd, hdc);

            HFONT font = CreateFont(height, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE,
                                    DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
                                    ANTIALIASED_QUALITY, DEFAULT_PITCH, NULL);

            SendMessage(charHandle, WM_SETFONT, (WPARAM) font, TRUE);
            ShowWindow(ctrlHandle, SW_HIDE);
            ShowWindow(altHandle, SW_HIDE);
            ShowWindow(shiftHandle, SW_HIDE);
            break;
        case WM_DESTROY:
            PostQuitMessage(0);
            break;
    }

    return DefWindowProcW(hwnd, msg, wParam, lParam);
}

LRESULT CALLBACK KeyboardProc(int code, WPARAM wParam, LPARAM lParam) {
    if (code != HC_ACTION) goto finish;

    KBDLLHOOKSTRUCT *kbd = (KBDLLHOOKSTRUCT *) lParam;

    if ((kbd->vkCode == VK_CONTROL || kbd->vkCode == VK_LCONTROL || kbd->vkCode == VK_RCONTROL) && ctrlHandle) {
        ShowWindow(ctrlHandle, wParam == WM_KEYDOWN ? SW_SHOW : SW_HIDE);
        goto finish;
    }

    if ((kbd->vkCode == VK_MENU || kbd->vkCode == VK_LMENU || kbd->vkCode == VK_RMENU) && ctrlHandle) {
        ShowWindow(altHandle, wParam == WM_SYSKEYDOWN ? SW_SHOW : SW_HIDE);
        goto finish;
    }

    if ((kbd->vkCode == VK_SHIFT || kbd->vkCode == VK_LSHIFT || kbd->vkCode == VK_RSHIFT) && ctrlHandle) {
        ShowWindow(shiftHandle, wParam == WM_KEYDOWN ? SW_SHOW : SW_HIDE);
        goto finish;
    }

    GetKeyState(0);
    if (wParam == WM_KEYDOWN || wParam == WM_SYSKEYDOWN) {
        BYTE keyboardState[256];
        if (!GetKeyboardState(keyboardState)) goto finish;

        wchar_t keyName[8];
        if (ToUnicode(kbd->vkCode, kbd->scanCode, keyboardState, keyName, 8, 0) && charHandle) {
            SendMessage(charHandle, WM_SETTEXT, (WPARAM) NULL, (LPARAM) keyName);
        }
    }

    finish:
    return CallNextHookEx(NULL, code, wParam, lParam);
}